import { useEffect, useState } from "react";
import { AxiosError } from "axios";
import { RoleModel } from "../dataProviders/common/Role/MapRoleList";
import { RoleProvider } from "../dataProviders/common/Role/RoleProvider";

const useGetRoleList = () => {
  const [data, setData] = useState<Array<RoleModel>>([]);
  const [error, setError] = useState<string | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  const handleOnCatch = (error: unknown) => {
    if (error instanceof AxiosError) {
      setError(error.response?.data.message);
      return;
    }
    if (error instanceof Error) {
      setError(error.message);
      return;
    }
    setError(error as string);
  };

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      try {
        const result: Array<RoleModel> = await RoleProvider.getList();

        setData(result);
      } catch (error: unknown) {
        handleOnCatch(error);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, []);

  return { data, error, isLoading };
};

export default useGetRoleList;
