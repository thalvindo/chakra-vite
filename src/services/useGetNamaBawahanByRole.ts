import { useEffect, useState } from "react";
import { AxiosError } from "axios";
import { UserProvider } from "../dataProviders/common/User/UserProvider";
import { UserModel } from "../dataProviders/common/User/MapUserByRole";
import { RoleModel } from "../dataProviders/common/Role/MapRoleList";

const useGetNamaBawahanByRole = (selectedRole: null | RoleModel) => {
  const [data, setData] = useState<Array<UserModel>>([]);
  const [error, setError] = useState<string | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  const handleOnCatch = (error: unknown) => {
    if (error instanceof AxiosError) {
      setError(error.response?.data.message);
      return;
    }
    if (error instanceof Error) {
      setError(error.message);
      return;
    }
    setError(error as string);
  };

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      try {
        if (!selectedRole) {
          setData([]);
          return;
        }
        const result: Array<UserModel> = await UserProvider.getList({
          role: selectedRole!!,
        });

        setData(result);
      } catch (error: unknown) {
        handleOnCatch(error);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, [selectedRole]);

  return { data, error, isLoading };
};

export default useGetNamaBawahanByRole;
