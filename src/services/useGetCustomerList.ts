import { useEffect, useState } from "react";
import { AxiosError } from "axios";
import { CustomerProvider } from "../dataProviders/common/Customer/CustomerProvider";
import { CustomerModel } from "../dataProviders/common/Customer/MapCustomerList";

const useGetCustomerList = () => {
  const [data, setData] = useState<Array<CustomerModel>>([]);
  const [error, setError] = useState<string | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  const handleOnCatch = (error: unknown) => {
    if (error instanceof AxiosError) {
      setError(error.response?.data.message);
      return;
    }
    if (error instanceof Error) {
      setError(error.message);
      return;
    }
    setError(error as string);
  };

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      try {
        const result: Array<CustomerModel> = await CustomerProvider.getList();

        setData(result);
      } catch (error: unknown) {
        handleOnCatch(error);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, []);

  return { data, error, isLoading };
};

export default useGetCustomerList;
