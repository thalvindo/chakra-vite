import { useEffect, useState } from "react";
import { OutletProvider } from "../dataProviders/common/Outlet/OutletProvider";
import { AxiosError } from "axios";
import { OutletModel } from "../dataProviders/common/Outlet/MapOutletList";

const useGetOutletsList = () => {
  const [data, setData] = useState<Array<OutletModel>>([]);
  const [error, setError] = useState<string | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  const handleOnCatch = (error: unknown) => {
    if (error instanceof AxiosError) {
      setError(error.response?.data.message);
      return;
    }
    if (error instanceof Error) {
      setError(error.message);
      return;
    }
    setError(error as string);
  };

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      try {
        const result: Array<OutletModel> = await OutletProvider.getList();

        setData(result);
      } catch (error: unknown) {
        handleOnCatch(error);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, []);

  return { data, error, isLoading };
};

export default useGetOutletsList;
