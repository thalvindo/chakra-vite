import Dexie from "dexie";
import { OutletModel } from "../dataProviders/common/Outlet/MapOutletList";
import { CustomerModel } from "../dataProviders/common/Customer/MapCustomerList";

export class IndexDB extends Dexie {
  // add table for each feature here.
  outletList: Dexie.Table<OutletModel, number>;
  customerList: Dexie.Table<CustomerModel, number>;
  approvalKlaimFilter: Dexie.Table<any, number>;

  constructor() {
    super("Ethical");
    // add attribute for each table here.
    this.version(2).stores({
      outletList: "id, name, code, sector, created_at, updated_at" + "code_post, address, group_territory",
      customerList: "id, name, code, specialist",
      approvalKlaimFilter: "++id, statusApproval.label, jabatan.name, bawahan.nama, customer.nama, periode",
    });

    this.outletList = this.table("outletList");
    this.customerList = this.table("customerList");
    this.approvalKlaimFilter = this.table("approvalKlaimFilter");
  }
}
const IndexedDatabase = new IndexDB();

export default IndexedDatabase;
