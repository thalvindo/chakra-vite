import React from "react";
import { Text, useTheme } from "@chakra-ui/react";

interface PageTitleProps {
  children: React.ReactNode;
  className?: string;
}

const PageTitle: React.FC<PageTitleProps> = (props) => {
  const theme = useTheme();
  return (
    <Text
      className={props.className}
      variant="h4"
      color={theme.colors.primary.main}
      fontWeight="bold"
      fontSize="4xl"
    >
      {props.children}
    </Text>
  );
};

export default PageTitle;
