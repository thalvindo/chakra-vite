import { Stack, Box, Button, Text, NumberInput, NumberInputField } from "@chakra-ui/react";
import { ArrowBackIcon, ArrowForwardIcon } from "@chakra-ui/icons";
import React from "react";
import Select, { SingleValue } from "react-select";
import { COMMON_PAGINATION_LIMIT_CHOICES } from "../../constants/constants";

type PaginationType = {
  showLimit?: boolean;
  showCurrentPageDetail?: boolean;
  handleOnLimitChange?: (value: number) => void;
  handleOnBack?: () => void;
  handleOnNext?: () => void;
  limit?: number;
  page?: number;
  totalPage?: number;
  totalData?: number;
};

type eachPaginationType = {
  value: number;
};
const Pagination = ({
  showLimit = true,
  showCurrentPageDetail = true,
  handleOnLimitChange = () => {},
  handleOnBack = () => {},
  handleOnNext = () => {},
  limit = 10,
  page = 1,
  totalPage = 1,
  totalData = 1,
}: PaginationType) => {
  return (
    <Stack sx={{ display: "flex", justifyContent: "flex-end", alignItems: "center", flexDirection: "row" }}>
      {showLimit && (
        <Stack sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          <Text>Rows Per Page:</Text>
          <Select
            options={COMMON_PAGINATION_LIMIT_CHOICES}
            onChange={(
              eachLimit: SingleValue<{
                value: number;
              }>
            ) => {
              handleOnLimitChange(eachLimit?.value!!);
            }}
            getOptionLabel={(eachLimit: eachPaginationType) => `${eachLimit.value}`}
            getOptionValue={(eachLimit: eachPaginationType) => `${eachLimit.value}`}
            placeholder={null}
            defaultValue={{ value: 10 }}
            value={{ value: limit }}
          />
        </Stack>
      )}
      {showCurrentPageDetail && (
        <Stack sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          <Text>
            {page * limit - (limit - 1)}-{page === totalPage ? totalData : page * limit}
          </Text>
          <Text> of </Text>
          <Text>{totalData}</Text>
        </Stack>
      )}
      <Stack sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <Button variant="ghost" sx={{ borderRadius: "40px" }} isDisabled={page <= 1} onClick={handleOnBack}>
          <ArrowBackIcon boxSize={4} />
        </Button>
        <Text>
          {page} / {totalPage}
        </Text>
        <Button variant="ghost" sx={{ borderRadius: "40px" }} onClick={handleOnNext} isDisabled={page === totalPage}>
          <ArrowForwardIcon boxSize={4} />
        </Button>
      </Stack>
    </Stack>
  );
};

export default Pagination;
