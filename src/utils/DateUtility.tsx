import dayjs, { Dayjs } from "dayjs";

export default class DateUtility {
  // time wasted gaslighting yourself from this logic : 2.

  // handle from Local time to Jakarta time (equivalent time in jakarta time utc)
  // eg :
  // jakarta utc + 7,
  // singapore utc + 8
  // send 00:00 from singapore.
  // after using this function,
  //singapore will see 01:00,
  // jakarta will see 00:00.
  // this function sends the same hour, but instead of their local timezone, we're sending jakarta timezone.
  static handleOffsetDifference = (dateInEpoch: number) => {
    const jakartaOffset = 420;
    const localOffset = dayjs().utcOffset();

    const diff = jakartaOffset - localOffset;

    let result = dateInEpoch - diff * 60;

    return result;
  };

  // handle from Jakarta time to Local time (equivalent time in Jakarta time utc)
  // eg :
  // jakarta utc + 7,
  // singapore utc + 8
  // backend/server sends 00:00 jakarta time.
  // after using this function,
  // singapore will see 00:00,
  // a place where time is utc + 6, will see it also as 00:00,
  // jakarta will see 00:00. (because it's jakarta time from the server)
  // this function sends same hour based on their local timezone.
  static handleOffsetDifferenceGet = (dateInEpoch: number) => {
    const jakartaOffset = 420;
    const localOffset = dayjs().utcOffset();

    const diff = jakartaOffset - localOffset;

    let result = dateInEpoch + diff * 60;

    return result;
  };

  static getCurrentTimeAsEpoch = () => {
    return dayjs().unix();
  };

  static getCurrentTimeAsDateObject = () => {
    return dayjs();
  };

  static getCurrentTimeAsFormat = (format: string) => {
    return dayjs().format(format);
  };

  static getObjectStartOfCurrentMonth = () => {
    return dayjs().startOf("month");
  };

  static getObjectEndOfCurrentMonth = () => {
    return dayjs().endOf("month");
  };

  static getEpochStartOfCurrentMonth = () => {
    return dayjs().startOf("month").unix();
  };

  static getEpochEndOfCurrentMonth = () => {
    return dayjs().endOf("month").unix();
  };

  static getEpochStartOfSelectedEpochDate = (epochTimestamp: number) => {
    const objectFormat = dayjs.unix(epochTimestamp);
    const startOfMonth = objectFormat.startOf("month");
    return startOfMonth.unix() as number;
  };

  static getEpochEndOfSelectedEpochDate = (epochTimestamp: number) => {
    const objectFormat = dayjs.unix(epochTimestamp);
    const endOfMonth = objectFormat.endOf("month");
    return endOfMonth.unix() as number;
  };
  // static getObjectStartOfSelectedEpochDate = (epochTimestamp: number) => {
  //   return dayjs.unix(epochTimestamp).startOf("month");
  // };

  // static getObjectEndOfSelectedEpochDate = (epochTimestamp: number) => {
  //   return dayjs.unix(epochTimestamp).endOf("month");
  // };

  static epochToDateObject = (epochTimestamp: number) => {
    const formattedDate = dayjs.unix(epochTimestamp);
    return formattedDate;
  };

  static dateObjectToEpoch = (dateObject: Dayjs) => {
    const formattedDate = dateObject.unix();
    return formattedDate;
  };

  static dateObjectToStringFormat = (dateObject: Dayjs, dateFormat: string) => {
    return dateObject.format(dateFormat);
  };

  static epochToStringFormat = (epochTimestamp: number, dateFormat: string) => {
    const formattedDate: Dayjs = dayjs.unix(epochTimestamp);
    return formattedDate.format(dateFormat);
  };
}
