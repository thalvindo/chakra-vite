const RemoveDuplicateInArray = (arr: any) => {
  return arr.filter((item: any, index: number) => arr.indexOf(item) === index);
};

export default RemoveDuplicateInArray;
