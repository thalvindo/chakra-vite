export const SectorConverter = (sector: string) => {
  if (sector === "clinic") return "Praktek Dokter";
  if (sector === "hospital") return "Rumah Sakit";
  if (sector === "pharmacy") return "Apotek";
  if (sector === "pharmacy_panel") return "Apotek Panel";
  return sector;
};
