type LoggedUser = {
  userId: number;
  userAuthId: number;
  userName: string;
  userRoleId: number;
  userRoleName: string;
  userRoleLabel: string;
  userEmail: string;
  userNip: string;
};

export default LoggedUser;
