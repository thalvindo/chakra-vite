import DateUtility from "../utils/DateUtility";

class DateEthical {
  private date: number;

  constructor(value: number) {
    this.date = DateUtility.handleOffsetDifferenceGet(value);
  }

  toJSON() {
    return DateUtility.handleOffsetDifference(this.date);
  }

  getEpochDate = () => {
    return this.date;
  };

  getObjectDate = () => {
    return DateUtility.epochToDateObject(this.date);
  };

  getStringDateFormat = (format: string) => {
    return DateUtility.epochToStringFormat(this.date, format);
  };

  getStartOfMonth = () => {
    return DateUtility.getEpochStartOfSelectedEpochDate(this.date);
  };

  getEndOfMonth = () => {
    return DateUtility.getEpochEndOfSelectedEpochDate(this.date);
  };
}

export default DateEthical;
