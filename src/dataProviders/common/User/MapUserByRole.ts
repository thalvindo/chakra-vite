import { isEmpty } from "lodash";

export class GENERALMESSAGE {
  message!: string;
  status!: number;
  code!: string;
  constructor(json: any) {
    this.mapFromJson(json);
  }
  mapFromJson(json: any) {
    this.message = json.data["message"];
    this.status = json.data["status"];
    this.code = json.data["code"];
  }
};

export class GetUserListModelPack extends GENERALMESSAGE {
  data!: UserModel[];

  constructor(json: any) {
    super(json);
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    var temp: UserModel[] = [];

    if (!isEmpty(json.data["data"])) {
      json.data.data?.map((eachJson: any) => {
        temp.push(new UserModel(eachJson));
      });
    }

    this.data = temp;
  }
}

export class UserModel {
  nip!: string;
  roleLabel!: string;
  roleName!: string;
  userName!: string;
  zoneType!: number;
  roleId!: number;
  userId!: number;
  zoneId!: number;

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.nip = json["nip"];
    this.roleLabel = json["role_label"];
    this.roleName = json["role_name"];
    this.userName = json["user_name"];
    this.zoneType = json["role_label"];
    this.roleId = json["role_id"];
    this.userId = json["user_id"];
    this.zoneId = json["zone_id"];
  }
}
