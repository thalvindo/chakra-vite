import { AxiosResponse } from "axios";
import {
  AXIOS_MESSAGE_RESPONSE,
  ERRORS,
  ETHICAL_API_VERSION,
} from "../../../constants/constants";
import AxiosBaseClient from "../../config/AxiosBaseClient";
import { GetUserListModelPack } from "./MapUserByRole";
import { RoleModel } from "../Role/MapRoleList";

const { ERROR_WHEN_FETCHING_DATA } = ERRORS;
const { SUCCESS_GET_STATUS, SUCCESS_MESSAGE } = AXIOS_MESSAGE_RESPONSE;
export const UserProvider = {
  getList: async (params: { role: RoleModel }): Promise<any> => {
    const response: AxiosResponse = await AxiosBaseClient.get(
      ETHICAL_API_VERSION + `/user/child`,
      {
        params: {
          role_name: params.role.name,
        },
      }
    );

    const userPackedResponse = new GetUserListModelPack(response);

    if (
      userPackedResponse.status === SUCCESS_GET_STATUS &&
      userPackedResponse.message === SUCCESS_MESSAGE
    )
      return Promise.resolve(userPackedResponse.data);

    return Promise.reject(new Error(ERROR_WHEN_FETCHING_DATA));
  },
};
