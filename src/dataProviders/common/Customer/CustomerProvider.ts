import { AxiosResponse } from "axios";
import {
  AXIOS_MESSAGE_RESPONSE,
  ERRORS,
  ETHICAL_API_VERSION,
} from "../../../constants/constants";
import AxiosBaseClient from "../../config/AxiosBaseClient";
import IndexedDatabase from "../../../database/IndexDB";
import { isEmpty } from "lodash";
import { CustomerModel, GetCustomerListModelPack } from "./MapCustomerList";

const { ERROR_WHEN_FETCHING_DATA } = ERRORS;
const { SUCCESS_GET_STATUS, SUCCESS_MESSAGE } = AXIOS_MESSAGE_RESPONSE;
export const CustomerProvider = {
  getList: async (): Promise<any> => {
    if (!IndexedDatabase.isOpen()) await IndexedDatabase.open();
    const databaseResponse = await IndexedDatabase.customerList.toArray();
    if (databaseResponse && !isEmpty(databaseResponse)) {
      return Promise.resolve(databaseResponse);
    }

    await IndexedDatabase.customerList.clear();
    const response: AxiosResponse = await AxiosBaseClient.get(
      ETHICAL_API_VERSION + `/customer/outlet/user`
    );

    const customerPackedResponse = new GetCustomerListModelPack(response);

    customerPackedResponse.data?.map((eachData: CustomerModel) => {
      IndexedDatabase.customerList.add(eachData);
    });

    if (
      customerPackedResponse.status === SUCCESS_GET_STATUS &&
      customerPackedResponse.message === SUCCESS_MESSAGE
    )
      return Promise.resolve(customerPackedResponse.data);

    return Promise.reject(new Error(ERROR_WHEN_FETCHING_DATA));
  },
};
