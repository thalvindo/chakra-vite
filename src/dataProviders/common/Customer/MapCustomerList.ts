import { isEmpty } from "lodash";

export class GetCustomerListModelPack {
  message!: string;
  status!: number;
  code!: string;
  data!: CustomerModel[];

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.message = json.data["message"];
    this.status = json.data["status"];
    this.code = json.data["code"];

    var temp: CustomerModel[] = [];

    if (!isEmpty(json.data["data"])) {
      json.data.data?.map((eachJson: any) => {
        temp.push(new CustomerModel(eachJson));
      });
    }

    this.data = temp;
  }
}

export class CustomerModel {
  id!: number;
  code!: string;
  name!: string;
  specialist!: string;

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.id = json["customer_id"];
    this.name = json["customer_name"];
    this.code = json["customer_code"];
    this.specialist = json["specialis"];
  }
}
