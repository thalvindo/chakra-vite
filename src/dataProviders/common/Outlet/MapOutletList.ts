import { isEmpty } from "lodash";
import { SectorConverter } from "../../../utils/SectorConverter";

export class GetOutletListModelPack {
  message!: string;
  status!: number;
  code!: string;
  data!: OutletModel[];

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.message = json.data["message"];
    this.status = json.data["status"];
    this.code = json.data["code"];

    var temp: OutletModel[] = [];

    if (!isEmpty(json.data["data"])) {
      json.data.data?.map((eachJson: any) => {
        temp.push(new OutletModel(eachJson));
      });
    }

    this.data = temp;
  }
}

export class OutletModel {
  created_at!: number;
  updated_at!: number | null;
  address!: string;
  code_post!: string;
  id!: number;
  name!: string;
  code!: string;
  sector!: string;

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.created_at = json["created_at"]; 
    this.updated_at = json["updated_at"];
    this.address = json["address"];
    this.code_post = json["code_post"];
    this.id = json["id"];
    this.name = json["name"];
    this.code = json["outlet_code"];
    this.sector = SectorConverter(json["sector_type"]);
  }

}

