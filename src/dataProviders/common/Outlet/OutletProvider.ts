import { AxiosResponse } from "axios";
import {
  AXIOS_MESSAGE_RESPONSE,
  ERRORS,
  ETHICAL_API_VERSION,
} from "../../../constants/constants";
import AxiosBaseClient from "../../config/AxiosBaseClient";
import { GetOutletListModelPack, OutletModel } from "./MapOutletList";
import IndexedDatabase from "../../../database/IndexDB";
import { isEmpty } from "lodash";

const { ERROR_WHEN_FETCHING_DATA } = ERRORS;
const { SUCCESS_GET_STATUS, SUCCESS_MESSAGE } = AXIOS_MESSAGE_RESPONSE;
export const OutletProvider = {
  getList: async (): Promise<any> => {
    if (!IndexedDatabase.isOpen()) await IndexedDatabase.open();
    const databaseResponse = await IndexedDatabase.outletList.toArray();
    if (databaseResponse && !isEmpty(databaseResponse)) {
      return Promise.resolve(databaseResponse);
    }

    await IndexedDatabase.outletList.clear();
    const response: AxiosResponse = await AxiosBaseClient.get(
      ETHICAL_API_VERSION + `/outlet/by/user`
    );

    const outletPackedResponse = new GetOutletListModelPack(response);

    outletPackedResponse.data?.map((eachData: OutletModel) => {
      IndexedDatabase.outletList.add(eachData);
    });

    if (
      outletPackedResponse.status === SUCCESS_GET_STATUS &&
      outletPackedResponse.message === SUCCESS_MESSAGE
    )
      return Promise.resolve(outletPackedResponse.data);

    return Promise.reject(new Error(ERROR_WHEN_FETCHING_DATA));
  },
};
