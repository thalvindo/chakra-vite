import { isEmpty } from "lodash";

export class GetRoleListModelPack {
  message!: string;
  status!: number;
  code!: string;
  data!: RoleModel[];

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.message = json.data["message"];
    this.status = json.data["status"];
    this.code = json.data["code"];

    var temp: RoleModel[] = [];

    if (!isEmpty(json.data["data"])) {
      json.data.data?.map((eachJson: any) => {
        temp.push(new RoleModel(eachJson));
      });
    }

    this.data = temp;
  }
}

export class RoleModel {
  created_at!: number;
  updated_at!: number | null;
  id!: number;
  name!: string;
  label!: string;
  parentId!: number;
  parentRole!: string | null;

  constructor(json: any) {
    this.mapFromJson(json);
  }

  mapFromJson(json: any) {
    this.created_at = json["created_at"];
    this.updated_at = json["updated_at"];
    this.id = json["role_id"];
    this.name = json["role_name"];
    this.label = json["role_label"];
    this.parentId = json["parentId"];
    this.parentRole = json["parent_role"];
  }
}
