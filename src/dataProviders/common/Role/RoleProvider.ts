import { AxiosResponse } from "axios";
import {
  AXIOS_MESSAGE_RESPONSE,
  ERRORS,
  ETHICAL_API_VERSION,
} from "../../../constants/constants";
import AxiosBaseClient from "../../config/AxiosBaseClient";
import { GetRoleListModelPack } from "./MapRoleList";

const { ERROR_WHEN_FETCHING_DATA } = ERRORS;
const { SUCCESS_GET_STATUS, SUCCESS_MESSAGE } = AXIOS_MESSAGE_RESPONSE;
export const RoleProvider = {
  getList: async (): Promise<any> => {
    const response: AxiosResponse = await AxiosBaseClient.get(
      ETHICAL_API_VERSION + `/role/child`
    );

    const rolePackedResponse = new GetRoleListModelPack(response);

    if (
      rolePackedResponse.status === SUCCESS_GET_STATUS &&
      rolePackedResponse.message === SUCCESS_MESSAGE
    )
      return Promise.resolve(rolePackedResponse.data);

    return Promise.reject(new Error(ERROR_WHEN_FETCHING_DATA));
  },
};
