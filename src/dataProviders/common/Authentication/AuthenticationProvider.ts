import { AxiosResponse } from "axios";
import AxiosAuthClient from "../../config/AxiosAuthClient";
import {
  AUTHENTICATION_API_ROOT,
  AXIOS_MESSAGE_RESPONSE,
  ERRORS,
  ETHICAL_API_VERSION,
} from "../../../constants/constants";
import AxiosBaseClient from "../../config/AxiosBaseClient";
import { MapUserData } from "./MapUserData";
import LoggedUser from "../../../models/common/LoggedUser";

const { SUCCESS_GET_STATUS, SUCCESS_MESSAGE } = AXIOS_MESSAGE_RESPONSE;
export const AuthenticationProvider = {
  login: async (param: any) => {
    const response: AxiosResponse = await AxiosAuthClient.post(
      ETHICAL_API_VERSION + AUTHENTICATION_API_ROOT + "/login",
      {
        email: param.email,
        password: param.password,
        remember_me: false,
      }
    );
    if (response.status === SUCCESS_GET_STATUS)
      return Promise.resolve(response.data.message);
    return Promise.reject(
      new Error(ERRORS.SOMETHING_WENT_WRONG + ", " + response.data.message)
    );
  },

  logout: async () => {
    const response: AxiosResponse = await AxiosAuthClient.post(
      ETHICAL_API_VERSION + AUTHENTICATION_API_ROOT + "/logout"
    );
    if (response.status === SUCCESS_GET_STATUS)
      return Promise.resolve(response.data.message);

    return Promise.reject(
      new Error(ERRORS.SOMETHING_WENT_WRONG + ", " + response.data.message)
    );
  },

  generateNewToken: async () => {
    const response: AxiosResponse = await AxiosAuthClient.post(
      ETHICAL_API_VERSION + AUTHENTICATION_API_ROOT + "/generate_token"
    );
    if (response.data.message === SUCCESS_MESSAGE)
      return Promise.resolve(response.data.message);

    return Promise.reject(response.data.message);
  },

  getUserProfile: async () => {
    const response: AxiosResponse = await AxiosBaseClient.get(
      ETHICAL_API_VERSION + "/user/profile"
    );

    const userData: LoggedUser = MapUserData(response);

    if (response.data.message === SUCCESS_MESSAGE)
      return Promise.resolve(userData ?? null);
    return Promise.reject(
      new Error(ERRORS.SOMETHING_WENT_WRONG + ", " + response.data.message)
    );
  },
};
