import { configureStore } from "@reduxjs/toolkit";
import AuthReducer from "./AuthReducer/AuthReducer";

const store = configureStore({
  reducer: {
    authReducer: AuthReducer,
  },
  devTools: process.env.NODE_ENV === "development",
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
