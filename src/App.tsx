import "./App.css";
import React from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { useAppDispatch } from "./redux/hooks";
import LoggedUser from "./models/common/LoggedUser";
import useGetUserProfile from "./features/Login/services/useGetUserProfile";
import WithSidebar from "./layout/Sidebar/WithSidebarLayout";
import { setLoggedUser } from "./redux/AuthReducer/AuthReducer";
import Login from "./features/Login/LoginPage";
import { AuthenticationProvider } from "./dataProviders/common/Authentication/AuthenticationProvider";
import ApprovalRencanaKunjunganPage from "./features/RencanaKunjungan/ApprovalRencanaKunjungan/RencanaKunjungan";
import { ACTIVITY_URL_PATH, ENTERTAIN_URL_PATH } from "./constants/constants";
import { Center } from "@chakra-ui/react";
import ApprovalKlaimPage from "./features/Entertain/ApprovalKlaim/ApprovalKlaimPage";

function App() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [appLoading, setAppLoading] = React.useState<boolean>(false);

  const fetchUserData = async () => {
    const data: LoggedUser = await useGetUserProfile();
    dispatch(setLoggedUser(data));
  };

  const getNewToken = async () => {
    try {
      await AuthenticationProvider.generateNewToken();
      await fetchUserData();
    } catch (error) {
      if (!location.pathname.includes("lupa-password")) {
        navigate("/login", { replace: true });
      }
    } finally {
      setAppLoading(false);
    }
  };

  React.useEffect(() => {
    setAppLoading(true);
    getNewToken();
  }, []);

  if (appLoading) return <h1>loading.</h1>;
  return (
    <>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route
          path="/"
          element={WithSidebar(
            <Center>
              <h1>Dashboard Not Implemented hehe</h1>
            </Center>
          )}
        />
        <Route
          path={ACTIVITY_URL_PATH.INPUT_RENCANA_KUNJUNGAN}
          element={WithSidebar(
            <Center>
              <h1>Input Rencana Kunjungan Not Implemented Hehe</h1>
            </Center>
          )}
        />
        <Route
          path={ACTIVITY_URL_PATH.APPROVAL_RENCANA_KUNJUNGAN}
          element={WithSidebar(<ApprovalRencanaKunjunganPage />)}
        />
        <Route
          path={ENTERTAIN_URL_PATH.APPROVAL_KLAIM_ENTERTAIN}
          element={WithSidebar(<ApprovalKlaimPage />)}
        />
      </Routes>
    </>
  );
}

export default App;
