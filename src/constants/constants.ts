import { CommonStatus } from "../models/common/Status";

export const ETHICAL_API_VERSION = "/v1";
export const AUTHENTICATION_API_ROOT = "/authentication";

export const ACTIVITY_URL_PATH = {
  INPUT_RENCANA_KUNJUNGAN: "/input-rencana-kunjungan",
  APPROVAL_RENCANA_KUNJUNGAN: "/approval-rencana-kunjungan",
};

export const ENTERTAIN_URL_PATH = {
  APPROVAL_KLAIM_ENTERTAIN: "/approval-klaim-entertain",
};

export const ERRORS = {
  SOMETHING_WENT_WRONG: "Something went wrong !",
  ERROR_WHEN_FETCHING_DATA: "Error when fetching data !",
};

export const AXIOS_MESSAGE_RESPONSE = {
  SUCCESS_MESSAGE: "Success",
  SUCCESS_GET_STATUS: 200,
};

export type COMMON_STATUS_TYPE = { id: CommonStatus; name: CommonStatus };
export const COMMON_STATUS: Array<COMMON_STATUS_TYPE> = [
  {
    id: CommonStatus.Rejected,
    name: CommonStatus.Rejected,
  },
  {
    id: CommonStatus.Pending,
    name: CommonStatus.Pending,
  },
  {
    id: CommonStatus.Approved,
    name: CommonStatus.Approved,
  },
  {
    id: CommonStatus.Processed,
    name: CommonStatus.Processed,
  },
  {
    id: CommonStatus.Transferred,
    name: CommonStatus.Transferred,
  },
];

export const FINANCE_COMMON_STATUS: Array<{
  id: CommonStatus;
  name: CommonStatus;
}> = [
  {
    id: CommonStatus.Processed,
    name: CommonStatus.Processed,
  },
  {
    id: CommonStatus.Transferred,
    name: CommonStatus.Transferred,
  },
];

export const COMMON_PAGINATION_LIMIT_CHOICES = [{ value: 10 }, { value: 20 }, { value: 30 }, { value: 50 }, { value: 100 }];
