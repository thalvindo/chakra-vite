import { ThemeConfig } from "@chakra-ui/react";

export interface EthicalColors {
  primary: {
    main: string;
    light: string;
  };
  error: {
    main: string;
  };
  success: {
    main: string;
  };
  info: {
    main: string;
  };
  warning: {
    main: string;
  };
  background: {
    default: string;
  };
  common: {
    white: string;
  };
}

export interface EthicalBreakpoints {
  tablet: string;
  smallMonitor: string;
  largeMonitor: string;
}

export interface EthicalTheme {
  colors: EthicalColors;
  breakpoints: EthicalBreakpoints;
}
