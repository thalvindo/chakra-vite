import { extendTheme, ThemeConfig, ThemeOverride } from "@chakra-ui/react";

const ethicalColors = {
  primary: {
    main: "#0F4C82",
    light: "#E6F0FC",
  },
  error: {
    main: "#B25550",
  },
  success: {
    main: "#4F7735",
  },
  info: {
    main: "#426EA4",
  },
  warning: {
    main: "#D6961C",
  },
  background: {
    default: "#F8F8FF",
  },
  common: {
    white: "#F8F8FF",
  },
};

const ethicalBreakpoints = {
  tablet: "800px",
  smallMonitor: "1024px",
  largeMonitor: "1280px",
};

// Add your color mode config
const config: ThemeConfig = {
  initialColorMode: "light",
  useSystemColorMode: false,
};

// Extend the theme with types
const theme = extendTheme({
  config,
  colors: ethicalColors,
  breakpoints: ethicalBreakpoints,
} satisfies ThemeOverride);

export default theme;
