export type MenuListModel = {
  title: string;
  pathName: string;
  icon?: JSX.Element;
};
