import { Box, Stack } from "@chakra-ui/react";
import Sidebar from "./Sidebar";

const WithSidebarLayout = ({ children }: { children: JSX.Element }) => (
  <>
    <Sidebar />
    <Stack sx={{ p: 10 }}>{children}</Stack>
  </>
);

const WithSidebar = (Component: JSX.Element) => (
  <WithSidebarLayout children={Component} />
);

export default WithSidebar;
