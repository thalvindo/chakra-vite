import { StarIcon } from "@chakra-ui/icons";
import {
  AccordionItem,
  AccordionButton,
  Stack,
  AccordionIcon,
  AccordionPanel,
  Box,
  Text,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { ACTIVITY_URL_PATH } from "../../../constants/constants";
import { MenuListModel } from "../models/MenuListModel";

const ActivityMenu = ({ onClose }: { onClose: Function }) => {
  const navigate = useNavigate();

  const handleOnClick = (pathName: string) => {
    navigate(pathName);
    onClose();
  };

  const ActivityMenuList: Array<MenuListModel> = [
    {
      title: "Input Rencana Kunjungan",
      pathName: ACTIVITY_URL_PATH.INPUT_RENCANA_KUNJUNGAN,
    },
    {
      title: "Approval Rencana Kunjungan",
      pathName: ACTIVITY_URL_PATH.APPROVAL_RENCANA_KUNJUNGAN,
      icon: <StarIcon />,
    },
    {
      title: "Lainnya",
      pathName: ACTIVITY_URL_PATH.APPROVAL_RENCANA_KUNJUNGAN,
    },
  ];

  return (
    <AccordionItem>
      <AccordionButton>
        <Stack
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <Stack
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: 4,
            }}
          >
            <StarIcon />
            <Box as="span" flex="1" textAlign="left">
              Activity
            </Box>
          </Stack>
          <AccordionIcon />
        </Stack>
      </AccordionButton>
      {ActivityMenuList.map((eachMenu, key) => (
        // tar jadiin component sendiri.
        <AccordionPanel pb={2} key={key}>
          <AccordionButton>
            <Stack
              sx={{
                flexDirection: "row",
                alignItems: "center",
                gap: 4,
              }}
              onClick={() => handleOnClick(eachMenu.pathName)}
            >
              {eachMenu.icon}
              <Text sx={{ width: "100%" }}>{eachMenu.title}</Text>
            </Stack>
          </AccordionButton>
        </AccordionPanel>
      ))}
    </AccordionItem>
  );
};

export default ActivityMenu;
