import { StarIcon } from "@chakra-ui/icons";
import { AccordionItem, AccordionButton, Stack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

const DashboardMenu = ({ onClose }: { onClose: Function }) => {
  const navigate = useNavigate();

  const handleOnClick = () => {
    navigate("/");
    onClose();
  };

  return (
    <AccordionItem>
      <AccordionButton>
        <Stack
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 4,
          }}
          onClick={handleOnClick}
        >
          <StarIcon />
          <Text>Dashboard</Text>
        </Stack>
      </AccordionButton>
    </AccordionItem>
  );
};

export default DashboardMenu;
