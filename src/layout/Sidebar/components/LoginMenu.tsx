import { StarIcon } from "@chakra-ui/icons";
import { AccordionItem, AccordionButton, Stack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

const LoginMenu = ({ onClose }: { onClose: Function }) => {
  const navigate = useNavigate();
  const handleOnClickLogin = () => {
    navigate("/login");
    onClose();
  };
  return (
    <AccordionItem>
      <AccordionButton>
        <Stack
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 4,
          }}
          onClick={handleOnClickLogin}
        >
          <StarIcon />
          <Text>Login disini dulu ya</Text>
        </Stack>
      </AccordionButton>
    </AccordionItem>
  );
};

export default LoginMenu;
