import { StarIcon } from "@chakra-ui/icons";
import {
  AccordionItem,
  AccordionButton,
  Stack,
  Text,
  useToast,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { AuthenticationProvider } from "../../../dataProviders/common/Authentication/AuthenticationProvider";
import { useAppDispatch } from "../../../redux/hooks";
import { clearLoggedUser } from "../../../redux/AuthReducer/AuthReducer";

const LogoutMenu = ({ onClose }: { onClose: Function }) => {
  const navigate = useNavigate();
  const toast = useToast();
  const dispatch = useAppDispatch();

  const handleOnLogoutSuccess = () => {
    toast({
      position: "bottom-right",
      title: "Sukses Logout",
      status: "success",
      duration: 5000,
      isClosable: true,
    });
    dispatch(clearLoggedUser());
  };

  const handleOnReject = () => {
    toast({
      position: "bottom-right",
      title: "Logout Gagal",
      status: "error",
      duration: 5000,
      isClosable: true,
    });
  };

  const handleOnClickLogout = async () => {
    await AuthenticationProvider.logout()
      .then(handleOnLogoutSuccess)
      .catch(handleOnReject);
    navigate("/login");
    onClose();
  };

  return (
    <AccordionItem>
      <AccordionButton>
        <Stack
          sx={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: 4,
          }}
          onClick={handleOnClickLogout}
        >
          <StarIcon />
          <Text>Logout disini dulu</Text>
        </Stack>
      </AccordionButton>
    </AccordionItem>
  );
};

export default LogoutMenu;
