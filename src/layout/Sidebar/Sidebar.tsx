import {
  useDisclosure,
  Button,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  DrawerHeader,
  DrawerBody,
  DrawerFooter,
  Accordion,
  Stack,
  Box,
  useTheme,
} from "@chakra-ui/react";
import React from "react";
import ActivityMenu from "./components/ActivityMenu";
import DashboardMenu from "./components/DashboardMenu";
import { HamburgerIcon } from "@chakra-ui/icons";
import LoginMenu from "./components/LoginMenu";
import LogoutMenu from "./components/LogoutMenu";
import EntertainMenu from "./components/EntertainMenu";

const Sidebar = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const sideBarRef = React.useRef<HTMLInputElement | null>(null);
  const theme = useTheme();
  const primaryTheme = theme.colors.primary.main;

  return (
    <>
      <Stack
        sx={{
          display: "flex",
          justifyContent: "space-between",
          flexDirection: "row",
          backgroundColor: primaryTheme,
          height: 12,
          paddingX: 2,
        }}
      >
        <Box>
          <Button
            // @ts-ignore
            ref={sideBarRef}
            colorScheme={primaryTheme}
            onClick={onOpen}
          >
            <HamburgerIcon boxSize={6} />
          </Button>
        </Box>
        <Box sx={{ gap: 5, display: "flex" }}>
          <Button colorScheme={primaryTheme} onClick={onOpen}>
            Mode
          </Button>
          <Button colorScheme={primaryTheme} onClick={onOpen}>
            Profile
          </Button>
        </Box>
      </Stack>
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        finalFocusRef={sideBarRef}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Ethical</DrawerHeader>
          <DrawerBody>
            <Accordion defaultIndex={[0]} allowMultiple>
              <DashboardMenu onClose={onClose} />
              <ActivityMenu onClose={onClose} />
              <EntertainMenu onClose={onClose} />
              <LoginMenu onClose={onClose} />
              <LogoutMenu onClose={onClose} />
            </Accordion>
          </DrawerBody>
          <DrawerFooter>Version berapa ini yach</DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default Sidebar;
