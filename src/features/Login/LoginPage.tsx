import { Stack, Button, Center, useToast } from "@chakra-ui/react";
import { useForm, FormProvider } from "react-hook-form";
import EmailField from "./components/EmailField";
import PasswordField from "./components/PasswordField";
import { useNavigate } from "react-router-dom";
import React from "react";
import IndexedDatabase from "../../database/IndexDB";
import LoggedUser from "../../models/common/LoggedUser";
import { setLoggedUser } from "../../redux/AuthReducer/AuthReducer";
import useGetUserProfile from "./services/useGetUserProfile";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { AXIOS_MESSAGE_RESPONSE, ERRORS } from "../../constants/constants";
import usePostLogin from "./services/usePostLogin";

const Login = () => {
  const methods = useForm();
  const toast = useToast();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const loggedUser: LoggedUser | null = useAppSelector((store) => store.authReducer.loggedUser);

  React.useEffect(() => {
    if (loggedUser) {
      toast({
        position: "bottom-right",
        title: "auto navigate to dashboard",
        status: "warning",
        duration: 5000,
        isClosable: true,
      });
      navigate("/");
    }
  }, [loggedUser]);

  const handleOnResolve = async () => {
    try {
      const data: LoggedUser = await useGetUserProfile();
      dispatch(setLoggedUser(data));
      if (!IndexedDatabase.isOpen()) await IndexedDatabase.open();
      toast({
        position: "bottom-right",
        title: "Sukses Login",
        status: "success",
        duration: 5000,
        isClosable: true,
      });
      navigate("/");
    } catch (error) {
      toast({
        position: "bottom-right",
        title: "Failed to Login",
        description: `${ERRORS.SOMETHING_WENT_WRONG}`,
        status: "error",
        duration: 5000,
        isClosable: true,
      });
    }
  };

  const handleOnReject = () => {
    toast({
      position: "bottom-right",
      title: "Failed to Login",
      description: `${ERRORS.SOMETHING_WENT_WRONG}`,
      status: "error",
      duration: 5000,
      isClosable: true,
    });
  };

  const handleOnSubmit = async (value: any) => {
    setIsLoading(true);
    try {
      const response = await usePostLogin({
        email: value.email,
        password: value.password,
        remember_me: false,
      });
      if (response === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE) {
        handleOnResolve();
      }
    } catch (e: unknown) {
      handleOnReject();
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Center>
      <Stack sx={{ display: "flex", justifyContent: "center", p: 6, width: "40%" }}>
        <FormProvider {...methods}>
          <form onSubmit={methods.handleSubmit(handleOnSubmit)}>
            <EmailField />
            <PasswordField />
            <Button mt={4} colorScheme="teal" isLoading={isLoading} type="submit">
              Submit
            </Button>
          </form>
        </FormProvider>
      </Stack>
    </Center>
  );
};

export default Login;
