import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  FormHelperText,
} from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";

const EmailField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  return (
    <FormControl isInvalid={!!errors.email}>
      <FormLabel>Email address</FormLabel>
      <Input
        type="email"
        {...register("email", {
          required: "This is required",
          minLength: {
            value: 5,
            message: "Minimum length should be 5",
          },
        })}
      />
      <FormErrorMessage>
        {errors.email && (errors.email.message as string)}
      </FormErrorMessage>
      <FormHelperText>We'll never share your email.</FormHelperText>
    </FormControl>
  );
};

export default EmailField;
