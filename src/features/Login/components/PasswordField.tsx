import {
  FormControl,
  FormLabel,
  Input,
  FormErrorMessage,
  FormHelperText,
} from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";

const PasswordField = () => {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  return (
    <FormControl isInvalid={!!errors.password}>
      <FormLabel>Password</FormLabel>
      <Input
        type="password"
        {...register("password", {
          required: "This is required",
          minLength: { value: 5, message: "Minimum length should be 5" },
        })}
      />
      <FormErrorMessage>
        {errors?.password && (errors?.password.message as string)}
      </FormErrorMessage>
      <FormHelperText>We'll never share your password :)</FormHelperText>
    </FormControl>
  );
};

export default PasswordField;
