import LoggedUser from "../../../models/common/LoggedUser";
import { AuthenticationProvider } from "../../../dataProviders/common/Authentication/AuthenticationProvider";

const useGetUserProfile = async () => {
  try {
    const response: LoggedUser = await AuthenticationProvider.getUserProfile();

    return Promise.resolve(response);
  } catch (error: any) {
    return Promise.reject(error);
  }
};

export default useGetUserProfile;
