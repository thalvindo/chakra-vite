import { AuthenticationProvider } from "../../../dataProviders/common/Authentication/AuthenticationProvider";
import { ERRORS } from "../../../constants/constants";

const usePostLogin = async (payload: {
  email: string;
  password: string;
  remember_me: boolean;
}) => {
  try {
    const response = await AuthenticationProvider.login(payload);

    if (response === "Success") {
      return response;
    }
    throw new Error(ERRORS.SOMETHING_WENT_WRONG);
  } catch (error: unknown) {
    return error;
  }
};

export default usePostLogin;
