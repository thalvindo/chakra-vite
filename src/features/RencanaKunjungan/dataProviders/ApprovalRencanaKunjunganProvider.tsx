import { AxiosResponse } from "axios";
import { ETHICAL_API_VERSION } from "../../../constants/constants";
import AxiosBaseClient from "../../../dataProviders/config/AxiosBaseClient";
import DateUtility from "../../../utils/DateUtility";

const {
  getEpochStartOfCurrentMonth,
  handleOffsetDifference,
  getEpochEndOfCurrentMonth,
} = DateUtility;

export const ApprovalRencanaKunjunganProvider = {
  getList: async (param: any) => {
    // const url = `/v1/header/approval/list?page=${params.pagination.page}&limit=${params.pagination.perPage}`;
    try {
      const response: AxiosResponse = await AxiosBaseClient.post(
        ETHICAL_API_VERSION + "/header" + "/approval/list",
        {
          start_period: handleOffsetDifference(getEpochStartOfCurrentMonth()),
          end_period: handleOffsetDifference(getEpochEndOfCurrentMonth()),
          bawahan: [],
          kunjungan: "Rencana",
          status: null,
        },
        {
          params: {
            page: 1,
            limit: 20,
          },
        }
      );

      if (response.status === 200) {
        return Promise.resolve(response.data);
      } else {
        // handle status lainnya gatau.
        return Promise.resolve(response.data);
      }
    } catch (error: any) {
      return Promise.reject(error.response);
    }
  },
};

// hasil response
// {
//   "message": "Success",
//   "status": 200,
//   "code": "SUCCESS",
//   "data": {
//       "metadata": {
//           "total_data": 197,
//           "total_data_per_page": 20,
//           "current_page": 1,
//           "previous_page": 0,
//           "total_page": 10,
//           "next_page_url": "https://develop-api.pharos.co.id/v1/header/approval/list?page=2&limit=20",
//           "previous_page_url": "https://develop-api.pharos.co.id/v1/header/approval/list?page=0&limit=20",
//           "first_page_url": "https://develop-api.pharos.co.id/v1/header/approval/list?page=1&limit=20",
//           "last_page_url": "https://develop-api.pharos.co.id/v1/header/approval/list?page=10&limit=20"
//       },
//       "records": [
//           {
//               "id": 0,
//               "user_id": 10,
//               "user_name": "PAMBEJO",
//               "user_auth": 10,
//               "user_nip": "P210218",
//               "user_email": "pambejo79@gmail.com",
//               "user_role_id": 7,
//               "user_status": "active",
//               "role_name": "field-force",
//               "role_label": "Field Force",
//               "role_parent_id": 6,
//               "approval_plan_id": null,
//               "approval_plan_status": "Pending",
//               "approval_plan_time": null,
//               "approval_realization_id": null,
//               "approval_realization_status": "Pending",
//               "approval_realization_time": null,
//               "description": null,
//               "start_date": null,
//               "end_date": null
//           },
//       ]
//   }
// }
