import React from "react";
import DesktopApprovalRencanaKunjungan from "./desktop/DesktopApprovalRencanaKunjungan";

const ApprovalRencanaKunjunganPage: React.FC = () => {
  return <DesktopApprovalRencanaKunjungan />;
};

export default ApprovalRencanaKunjunganPage;
