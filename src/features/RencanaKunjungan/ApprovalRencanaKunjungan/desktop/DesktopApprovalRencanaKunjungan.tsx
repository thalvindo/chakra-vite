import { Box, Stack, Text } from "@chakra-ui/react";
import PageTitle from "../../../../components/PageTitle";
import useGetOutletList from "../../../../services/useGetOutletList";

const DesktopApprovalRencanaKunjungan = () => {
  // React.useEffect(() => {
  //   const getListKunjungan = async () => {
  //     await ApprovalRencanaKunjunganProvider.getList({});
  //   };

  //   getListKunjungan();

  // }, []);

  return (
    <>
      <PageTitle>Approval Rencana Kunjungan</PageTitle>
      <Stack
        sx={{
          display: "flex",
          justifyContent: "space-between",
          flexDirection: "row",
          marginTop: 6,
        }}
      >
        <Box>
          <Text>bagian dropdown period</Text>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            flexDirection: "row",
            gap: 8,
          }}
        >
          <Box>
            <Text>show filter</Text>
          </Box>
          <Box>
            <Text>Export Excel</Text>
          </Box>
        </Box>
      </Stack>
      <Box>
        <Text>ini loopingan table</Text>
      </Box>
    </>
  );
};

export default DesktopApprovalRencanaKunjungan;
