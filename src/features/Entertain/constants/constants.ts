export const APPROVAL_CONSTANTS = {
  REJECT: "reject",
  APPROVE: "approve",
};

export const APPROVAL_CONSTANTS_URL = {
  REJECT: "reject",
  APPROVE: "approval",
};
