import { COMMON_STATUS_TYPE } from "../../../constants/constants";
import { CustomerModel } from "../../../dataProviders/common/Customer/MapCustomerList";
import { RoleModel } from "../../../dataProviders/common/Role/MapRoleList";
import { UserModel } from "../../../dataProviders/common/User/MapUserByRole";
import DateEthical from "../../../models/DateEthical";

type ConstructorProps = {
  jabatan: RoleModel | null;
  bawahan: UserModel | null;
  customer: CustomerModel | null;
  periode: any;
  statusApproval: COMMON_STATUS_TYPE | null;
};

class ApprovalFilterValues {
  jabatan: RoleModel | null;
  bawahan: UserModel | null;
  customer: CustomerModel | null;
  periode: DateEthical;
  statusApproval: COMMON_STATUS_TYPE | null;

  constructor({ jabatan, bawahan, customer, periode, statusApproval }: ConstructorProps) {
    this.jabatan = jabatan;
    this.bawahan = bawahan;
    this.customer = customer;
    this.periode = periode instanceof DateEthical ? periode : new DateEthical(periode);
    this.statusApproval = statusApproval;
  }
}

export default ApprovalFilterValues;
