import DateEthical from "../../../models/DateEthical";
import { CommonStatus } from "../../../models/common/Status";

class KlaimEntertainGetListModelPack {
  message: string;
  status: number;
  code: string;
  data: {
    metadata: KlaimEntertainListMetaDataModel;
    records: Array<KlaimEntertainModel>;
  };

  constructor(json: any) {
    let records: KlaimEntertainModel[] = [];

    if (json["data"]["records"] !== null) {
      json.data.records?.map((eachData: any) => {
        records.push(new KlaimEntertainModel(eachData));
      });
    }
    this.message = json.message;
    this.status = json.status;
    this.code = json.code;
    this.data = {
      metadata: new KlaimEntertainListMetaDataModel(json.data.metadata),
      records: records ?? [],
    };
  }
}

class KlaimEntertainListMetaDataModel {
  totalData: number;
  totalDataPerPage: number;
  currentPage: number;
  previousPage: number;
  totalPage: number;
  nextPageUrl: string;
  previousPageUrl: string;
  firstPageUrl: string;
  lastPageUrl: string;

  constructor(json: any) {
    this.totalData = json["total_data"];
    this.totalDataPerPage = json["total_data_per_page"];
    this.currentPage = json["current_page"];
    this.previousPage = json["previous_page"];
    this.totalPage = json["total_page"];
    this.nextPageUrl = json["next_page_url"];
    this.previousPageUrl = json["previous_page_url"];
    this.firstPageUrl = json["first_page_url"];
    this.lastPageUrl = json["last_page_url"];
  }
}

class KlaimEntertainModel {
  id: number;
  dateOfEntertain: DateEthical;
  createdAt: DateEthical;
  type: string;
  total: number;
  totalHistory: number;
  billPhotoUrl: string;
  keterangan: string;
  noteFinance: string;
  transferredTime: DateEthical;
  approvalDetail: KlaimEntertainApprovalDetailModel;
  entertainStatus: CommonStatus | undefined;
  supplierData: {
    supplierName: string;
    city: string;
    address: string;
  };
  customerData: {
    namaOutlet: string;
    kodeOutlet: string;
    namaCustomer: string;
    kodeCustomer: string;
    spesialis: string;
    jabatan: string;
  };
  entertainCreatedBy: {
    userName: string;
    userNip: string;
    userRoleName: string;
    userRoleLabel: string;
  };
  processedDetail: {
    orCode: string;
    orProcessedAt: DateEthical | undefined;
  };

  constructor(json: any) {
    this.id = json["entertain_id"];
    this.dateOfEntertain = new DateEthical(json.date_of_entertain);
    this.createdAt = new DateEthical(json.created_at);
    this.type = json["type"];
    this.total = json["total"];
    this.totalHistory = json["total_history"];
    this.billPhotoUrl = json["bill_photo"];
    this.keterangan = json["keterangan"];
    this.noteFinance = json["note_finance"];
    this.transferredTime = new DateEthical(json.transferred_time);

    this.approvalDetail = new KlaimEntertainApprovalDetailModel(json);

    this.entertainStatus = json["entertain_status"];
    this.supplierData = {
      supplierName: json["suplier_data"]["supplier_name"],
      city: json["suplier_data"]["city"],
      address: json["suplier_data"]["address"],
    };
    this.customerData = {
      namaOutlet: json["customer_data"]["outlet_name"],
      kodeOutlet: json["customer_data"]["outlet_code"],
      namaCustomer: json["customer_data"]["customer_name"],
      kodeCustomer: json["customer_data"]["customer_code"],
      spesialis: json["customer_data"]["customer_specialist"],
      jabatan: json["customer_data"]["customer_type"],
    };
    this.entertainCreatedBy = {
      userName: json["entertain_created_by"]["user_name"],
      userNip: json["entertain_created_by"]["user_nip"],
      userRoleName: json["entertain_created_by"]["user_role_name"],
      userRoleLabel: json["entertain_created_by"]["user_role_label"],
    };
    this.processedDetail = {
      orCode: json["processed_detail"]["or_code"],
      orProcessedAt: new DateEthical(json.or_processed_at),
    };
  }
}

class KlaimEntertainApprovalDetailModel {
  isApprovedBySm: boolean;
  isApprovableBySm: boolean;
  SmApproveAt: DateEthical | undefined;
  isApprovedByNsm: boolean;
  isApprovableByNsm: boolean;
  NsmApproveAt: DateEthical | undefined;
  isApprovedByMarkDir: boolean;
  isApprovableByMarkDir: boolean;
  MarkDirApproveAt: DateEthical | undefined;

  constructor(json: any) {
    this.isApprovedBySm = json["approve_sm"];
    this.isApprovableBySm = json["approve_sm_able"];
    this.SmApproveAt = new DateEthical(json.approve_sm_created_at);
    this.isApprovedByNsm = json["approve_nsm"];
    this.isApprovableByNsm = json["approve_nsm_able"];
    this.NsmApproveAt = new DateEthical(json.approve_nsm_created_at);
    this.isApprovedByMarkDir = json["approve_mark_dir"];
    this.isApprovableByMarkDir = json["approve_mark_dir_able"];
    this.MarkDirApproveAt = new DateEthical(json.approve_mark_dir_created_at);
  }
}

export { KlaimEntertainGetListModelPack, KlaimEntertainListMetaDataModel, KlaimEntertainModel };
