import { Dispatch, createContext, SetStateAction } from "react";
import filterInitialValue from "../utils/initialFilterValues";
import ApprovalFilterValues from "../models/ApprovalFilterValues";

interface ApprovalKlaimContextType {
  filterValues: ApprovalFilterValues;
  setFilterValues: Dispatch<SetStateAction<ApprovalFilterValues>>;
}

const initialValue: ApprovalKlaimContextType = {
  filterValues: filterInitialValue,
  setFilterValues: () => {},
};

const ApprovalKlaimContext = createContext<ApprovalKlaimContextType>(initialValue);

export default ApprovalKlaimContext;
