import { createContext } from "react";
import { KlaimEntertainModel } from "../models/KlaimEntertainListModelPack";

type ApprovalKlaimContextType = {
  record: KlaimEntertainModel | null;
  refetch: () => void;
};

const initialValue: ApprovalKlaimContextType = {
  record: null,
  refetch: () => {},
};

const ApprovalKlaimRecordContext = createContext<ApprovalKlaimContextType>(initialValue);

export default ApprovalKlaimRecordContext;
