import { Stack, Box, useTheme, useDisclosure, useMediaQuery, Grid, GridItem } from "@chakra-ui/react";
import React from "react";
import PageTitle from "../../../components/PageTitle";
import useGetEntertainList from "../services/useGetListApprovalEntertain";
import filterInitialValue from "../utils/initialFilterValues";
import ApprovalFilterValues from "../models/ApprovalFilterValues";
import FilterModal from "../components/FilterModal";
import ListActionButton from "../components/ListActionButton";
import FilterBoxArea from "../components/FilterBoxArea";
import ApprovalKlaimFilterContext from "../context/ApprovalKlaimFilterContext";
import ApprovalCard from "../components/ApprovalCard";
import useSaveFilter from "../services/useSaveFilter";
import useGetFilter from "../services/useGetFilter";
import ApprovalKlaimRecordContext from "../context/ApprovalKlaimRecordContext";
import Pagination from "../../../components/Pagination/Pagination";

const ApprovalKlaimPage: React.FC = () => {
  const { message, saveData } = useSaveFilter();
  const { data: filterPersistentData } = useGetFilter();

  const [filterValues, setFilterValues] = React.useState<ApprovalFilterValues>(filterInitialValue);
  const [page, setPage] = React.useState<number>(1);
  const [limit, setLimit] = React.useState<number>(10);

  const { data, metaData, error, isLoading, refetch } = useGetEntertainList(filterValues, { page: page, limit: limit });
  const theme = useTheme();

  const [isDesktopView] = useMediaQuery(`(min-width: ${theme.breakpoints.tablet})`);

  React.useEffect(() => {
    setFilterValues(filterPersistentData ?? filterInitialValue);
  }, [filterPersistentData]);

  const { isOpen, onOpen, onClose } = useDisclosure();

  React.useEffect(() => {
    document.title = "Approval Klaim Entertain";
  }, []);

  const handleOnLimitChange = (updatedLimit: number) => {
    setLimit(updatedLimit);
  };

  const handleOnNextPage = () => {
    setPage((prevPage) => prevPage + 1);
  };

  const handleOnPrevPage = () => {
    setPage((prevPage) => prevPage - 1);
  };

  console.log(page);

  return (
    <>
      <ApprovalKlaimFilterContext.Provider
        value={{
          filterValues: filterValues,
          setFilterValues: setFilterValues,
        }}
      >
        <PageTitle>Approval Klaim Entertain</PageTitle>
        <Stack
          sx={{
            display: "flex",
            justifyContent: "space-between",
            flexDirection: "row",
            marginTop: 6,
          }}
        >
          <FilterBoxArea />
          <ListActionButton onOpen={onOpen} />
        </Stack>
        <FilterModal
          isOpen={isOpen}
          onClose={onClose}
          saveData={saveData}
          hasCustomerFilter
          hasStatusFilter
          hasJabatanAndBawahanFilter
          hasSinglePeriodeFilter
        />
        <Box sx={{ flexDirection: "column" }}>
          {isLoading && <h1>loadingg ?</h1>}
          {error && <h1>{error}</h1>}

          <Grid templateColumns={`repeat(${isDesktopView ? 2 : 1}, 1fr)`} gap={6}>
            {data &&
              data.map((eachData) => {
                return (
                  <ApprovalKlaimRecordContext.Provider
                    value={{
                      record: eachData,
                      refetch: refetch,
                    }}
                    key={eachData.id}
                  >
                    <GridItem w="100%" m={2} key={eachData.id}>
                      <ApprovalCard key={eachData.id} />
                    </GridItem>
                  </ApprovalKlaimRecordContext.Provider>
                );
              })}
          </Grid>
        </Box>
        <Pagination
          handleOnLimitChange={handleOnLimitChange}
          handleOnBack={handleOnPrevPage}
          handleOnNext={handleOnNextPage}
          page={page}
          limit={limit}
          totalPage={metaData?.totalPage}
          totalData={metaData?.totalData}
          showLimit
          showCurrentPageDetail
        />
      </ApprovalKlaimFilterContext.Provider>
    </>
  );
};

export default ApprovalKlaimPage;
