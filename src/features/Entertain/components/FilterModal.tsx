import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Stack,
  Text,
  Center,
  useTheme,
  useMediaQuery,
} from "@chakra-ui/react";
import React from "react";
import ApprovalKlaimContext from "../context/ApprovalKlaimFilterContext";
import DateUtility from "../../../utils/DateUtility";
import { FormProvider, useForm } from "react-hook-form";
import FilterByCustomerName from "./Filter/FilterByCustomerName";
import FilterByStatus from "./Filter/FilterByStatus";
import FilterByJabatanAndBawahan from "./Filter/FilterByJabatanAndBawahan";
import FilterByPeriode from "./Filter/FilterByPeriode";
import ApprovalFilterValues from "../models/ApprovalFilterValues";

const FilterModal = ({
  isOpen,
  onClose,
  saveData,
  hasCustomerFilter = false,
  hasStatusFilter = false,
  hasJabatanAndBawahanFilter = false,
  hasSinglePeriodeFilter = false,
}: {
  isOpen: boolean;
  onClose: () => void;
  saveData: (filterData: ApprovalFilterValues) => void;
  hasCustomerFilter?: boolean;
  hasStatusFilter?: boolean;
  hasJabatanAndBawahanFilter?: boolean;
  hasSinglePeriodeFilter?: boolean;
}) => {
  const theme = useTheme();
  const [isDesktopView] = useMediaQuery(`(min-width: ${theme.breakpoints.tablet})`);

  const { filterValues, setFilterValues } = React.useContext(ApprovalKlaimContext);

  const methods = useForm({
    defaultValues: {
      jabatan: filterValues.jabatan,
      customer: filterValues.customer,
      bawahan: filterValues.bawahan,
      periode: DateUtility.getCurrentTimeAsEpoch(),
      statusApproval: filterValues.statusApproval,
    },
  });

  React.useEffect(() => {
    if (filterValues) {
      methods.setValue("customer", filterValues.customer);
      methods.setValue("jabatan", filterValues.jabatan);
      methods.setValue("bawahan", filterValues.bawahan);
      methods.setValue("statusApproval", filterValues.statusApproval);
      methods.setValue("periode", filterValues.periode.getEpochDate());
    }
  }, [filterValues]);

  const handleClearAll = () => {
    methods.setValue("customer", null);
    methods.setValue("jabatan", null);
    methods.setValue("bawahan", null);
    methods.setValue("statusApproval", null);
    methods.setValue("periode", DateUtility.getCurrentTimeAsEpoch());
    handleOnSubmitFilter();
  };

  const handleOnSubmitFilter = () => {
    const updatedFilter = new ApprovalFilterValues({
      jabatan: methods.getValues("jabatan"),
      bawahan: methods.getValues("bawahan"),
      customer: methods.getValues("customer"),
      statusApproval: methods.getValues("statusApproval"),
      periode: methods.getValues("periode"),
    });
    setFilterValues(updatedFilter);
    saveData(updatedFilter);
    onClose();
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose} isCentered autoFocus={false} size={isDesktopView ? "3xl" : "sm"}>
        <FormProvider {...methods}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>
              <Center
                sx={{
                  justifyContent: "space-between",
                  display: "flex",
                  gap: 10,
                }}
              >
                <Text>Filter Data</Text>
                <Button sx={{ textDecoration: "underline" }} variant="ghost" onClick={handleClearAll}>
                  Clear All
                </Button>
              </Center>
            </ModalHeader>
            <ModalBody>
              <Stack
                sx={{
                  justifyContent: "space-between",
                  display: "flex",
                  gap: 4,
                  flexDirection: "row",
                }}
              >
                <FilterByCustomerName hasCustomerFilter={hasCustomerFilter} />
                <FilterByStatus hasStatusFilter={hasStatusFilter} />
              </Stack>
              <FilterByJabatanAndBawahan hasJabatanAndBawahanFilter={hasJabatanAndBawahanFilter} />
              <FilterByPeriode hasSinglePeriodeFilter={hasSinglePeriodeFilter} />
            </ModalBody>
            <ModalFooter sx={{ justifyContent: "space-around", display: "flex", gap: 10 }}>
              <Button colorScheme="blue" onClick={onClose} sx={{ width: "50%" }} variant={"outline"}>
                Close
              </Button>
              <Button colorScheme={`blue`} sx={{ width: "50%", backgroundColor: `${theme.colors.primary.main}` }} onClick={handleOnSubmitFilter}>
                <Text>Submit</Text>
              </Button>
            </ModalFooter>
          </ModalContent>
        </FormProvider>
      </Modal>
    </>
  );
};

export default FilterModal;
