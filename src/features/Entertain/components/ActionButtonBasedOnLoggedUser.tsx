import { Button, Stack, useDisclosure, useToast } from "@chakra-ui/react";
import React from "react";
import ApprovalKlaimRecordContext from "../context/ApprovalKlaimRecordContext";
import LoggedUser from "../../../models/common/LoggedUser";
import { useAppSelector } from "../../../redux/hooks";
import { ROLE_ID_MARKETING_DIRECTOR, ROLE_ID_MSD, ROLE_ID_NSM, ROLE_ID_SM } from "../../../constants/RoleConstants";
import usePutApproval from "../services/usePutApproval";
import { APPROVAL_CONSTANTS } from "../constants/constants";
import { AXIOS_MESSAGE_RESPONSE, COMMON_STATUS } from "../../../constants/constants";
import { isNil } from "lodash";
import usePostProcessApprovalMsd from "../services/usePostProcessApprovalMsd";
import MsdProcessModal from "./Modal/MsdProcessModal";
import { CommonStatus } from "../../../models/common/Status";

const ActionButtonBasedOnLoggedUser = () => {
  const { record, refetch } = React.useContext(ApprovalKlaimRecordContext);
  const loggedUser: LoggedUser | null = useAppSelector((store) => store.authReducer.loggedUser);
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const { putApprovalEntertain, isLoading: isLoadingApproval } = usePutApproval();
  const { postProcessApproval } = usePostProcessApprovalMsd();

  const [isGeneralActionButtonShown, setIsGeneralActionButtonShown] = React.useState<boolean>(false);
  const [isMsdProcessButtonShown, setIsMsdProcessButtonShown] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (loggedUser) {
      if (record?.entertainStatus === CommonStatus.Pending) {
        if (loggedUser.userRoleId === ROLE_ID_SM && record?.approvalDetail.isApprovableBySm && isNil(record?.approvalDetail.isApprovedBySm)) {
          setIsGeneralActionButtonShown(true);
        } else if (
          // logic sm
          (record?.approvalDetail.isApprovableBySm || isNil(record?.approvalDetail.isApprovedBySm)) &&
          loggedUser.userRoleId === ROLE_ID_NSM &&
          // logic nsm
          record?.approvalDetail.isApprovableByNsm &&
          isNil(record?.approvalDetail.isApprovedByNsm)
        ) {
          setIsGeneralActionButtonShown(true);
        } else if (
          // logic sm
          (record?.approvalDetail.isApprovableBySm || isNil(record?.approvalDetail.isApprovedBySm)) &&
          // logic nsm
          (record?.approvalDetail.isApprovableByNsm || isNil(record?.approvalDetail.isApprovedByNsm)) &&
          // logic msd
          loggedUser.userRoleId === ROLE_ID_MARKETING_DIRECTOR &&
          record?.approvalDetail.isApprovableByMarkDir &&
          isNil(record?.approvalDetail.isApprovedByMarkDir)
        ) {
          setIsGeneralActionButtonShown(true);
        } else {
          setIsGeneralActionButtonShown(false);
        }
      }
      if (loggedUser.userRoleId === ROLE_ID_MSD && record?.entertainStatus === CommonStatus.Approved) {
        setIsMsdProcessButtonShown(true);
      } else {
        setIsMsdProcessButtonShown(false);
      }
    }
  }, [loggedUser, putApprovalEntertain, postProcessApproval]);

  const handleOnApprove = async () => {
    const response = await putApprovalEntertain(record!!, APPROVAL_CONSTANTS.APPROVE);
    if (response === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE) {
      toast({
        title: "Sukses Approve",
        status: "success",
      });
      refetch();
    } else {
      toast({
        title: "Gagal Approve",
        status: "warning",
      });
      refetch();
    }
  };
  const handleOnReject = async () => {
    const response = await putApprovalEntertain(record!!, APPROVAL_CONSTANTS.REJECT);
    if (response === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE) {
      toast({
        title: "Sukses Approve",
        status: "success",
      });
      refetch();
    } else {
      toast({
        title: "Gagal Approve",
        status: "warning",
      });
      refetch();
    }
  };

  return (
    <>
      {isGeneralActionButtonShown && (
        <Stack sx={{ display: "flex", flexDirection: "row", justifyContent: "flex-end" }}>
          <Button onClick={handleOnReject} isLoading={isLoadingApproval}>
            Reject
          </Button>
          <Button onClick={handleOnApprove} isLoading={isLoadingApproval}>
            Approve
          </Button>
        </Stack>
      )}
      {isMsdProcessButtonShown && (
        <Stack sx={{ display: "flex", flexDirection: "row", justifyContent: "flex-end" }}>
          <Button onClick={onOpen}>Process</Button>
        </Stack>
      )}
      <MsdProcessModal isOpen={isOpen} onClose={onClose} postProcessApproval={postProcessApproval} />
    </>
  );
};

export default ActionButtonBasedOnLoggedUser;
