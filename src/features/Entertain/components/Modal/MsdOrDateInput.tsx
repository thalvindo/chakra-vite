import { Input, Stack, Text } from "@chakra-ui/react";
import dayjs from "dayjs";
import React from "react";
import DateEthical from "../../../../models/DateEthical";
import DateUtility from "../../../../utils/DateUtility";
import { useFormContext } from "react-hook-form";

const MsdOrDateInput = () => {
  const formContext = useFormContext();
  return (
    <Stack>
      <Text>Tanggal Order Request</Text>
      <Input
        placeholder="Tanggal OR"
        size="md"
        type="date"
        isRequired
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          const formattedDate = new DateEthical(DateUtility.dateObjectToEpoch(dayjs(e.target.value)));
          formContext.setValue("orDate", formattedDate);
        }}
      />
    </Stack>
  );
};

export default MsdOrDateInput;
