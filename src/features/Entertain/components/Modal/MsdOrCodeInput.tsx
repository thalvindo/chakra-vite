import { Stack, Input, Text } from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";

const MsdOrCodeInput = () => {
  const formContext = useFormContext();
  return (
    <Stack>
      <Text>Order Request Code</Text>
      <Input placeholder="OR Code" onChange={(e) => formContext.setValue("orCode", e.target.value)} />
    </Stack>
  );
};

export default MsdOrCodeInput;
