import {
  useDisclosure,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Text,
  useTheme,
  Stack,
  Input,
  useToast,
} from "@chakra-ui/react";
import React from "react";
import { FormProvider, useForm } from "react-hook-form";
import MsdOrDateInput from "./MsdOrDateInput";
import ApprovalKlaimRecordContext from "../../context/ApprovalKlaimRecordContext";
import { KlaimEntertainModel } from "../../models/KlaimEntertainListModelPack";
import MsdOrCodeInput from "./MsdOrCodeInput";
import { AXIOS_MESSAGE_RESPONSE } from "../../../../constants/constants";

const MsdProcessModal = ({
  isOpen,
  onClose,
  postProcessApproval,
}: {
  isOpen: boolean;
  onClose: () => void;
  postProcessApproval: (record: KlaimEntertainModel, payload: any) => Promise<unknown>;
}) => {
  const { record, refetch } = React.useContext(ApprovalKlaimRecordContext);
  const theme = useTheme();
  const methods = useForm({});
  const toast = useToast();

  const handleOnSubmit = async () => {
    const payload = {
      or_code: methods.getValues("orCode"),
      or_process_date: methods.getValues("orDate"),
    };
    const response = await postProcessApproval(record!!, payload);
    if (response === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE) {
      toast({
        title: "Sukses Process OR",
        status: "success",
      });
      refetch();
      onClose();
    } else {
      toast({
        title: `Error Process OR. ${response}`,
        status: "warning",
      });
      refetch();
    }
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Input Order Request Data</ModalHeader>
          <ModalBody>
            <FormProvider {...methods}>
              <MsdOrCodeInput />
              <MsdOrDateInput />
            </FormProvider>
          </ModalBody>

          <ModalFooter sx={{ justifyContent: "space-around", display: "flex", gap: 10 }}>
            <Button colorScheme="blue" onClick={onClose} sx={{ width: "50%" }} variant={"outline"}>
              Close
            </Button>
            <Button colorScheme={`blue`} sx={{ width: "50%", backgroundColor: `${theme.colors.primary.main}` }} onClick={handleOnSubmit}>
              <Text>Submit</Text>
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default MsdProcessModal;
