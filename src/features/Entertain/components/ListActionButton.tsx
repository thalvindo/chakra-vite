import { Button, Box } from "@chakra-ui/react";

const ListActionButton = ({ onOpen }: { onOpen: () => void }) => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "row",
        gap: 8,
      }}
    >
      <Button onClick={onOpen} variant="ghost">
        Show Filter
      </Button>
      <Box>
        <Button onClick={() => console.log("clicked export")}>Export Excel</Button>
      </Box>
    </Box>
  );
};

export default ListActionButton;
