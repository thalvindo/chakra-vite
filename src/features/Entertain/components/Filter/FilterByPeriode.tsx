import React from "react";
import { Input, Stack, Text } from "@chakra-ui/react";
import dayjs from "dayjs";
import { useFormContext } from "react-hook-form";
import DateEthical from "../../../../models/DateEthical";
import DateUtility from "../../../../utils/DateUtility";
import ApprovalKlaimContext from "../../context/ApprovalKlaimFilterContext";

const FilterByPeriode = ({ hasSinglePeriodeFilter }: { hasSinglePeriodeFilter: boolean }) => {
  const formContext = useFormContext();
  const { filterValues } = React.useContext(ApprovalKlaimContext);

  const [periode, setPeriode] = React.useState<string>(filterValues.periode?.getStringDateFormat("YYYY-MM"));

  return (
    <>
      {hasSinglePeriodeFilter && (
        <Stack
          sx={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            gap: 2,
          }}
        >
          <Stack sx={{ width: "50%" }}>
            <Text sx={{ mb: 2 }}>Periode Awal</Text>
            <Input
              placeholder="Periode"
              size="md"
              type="month"
              isRequired
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                const formattedDate = new DateEthical(DateUtility.dateObjectToEpoch(dayjs(e.target.value)));
                formContext.setValue("periode", formattedDate);
                setPeriode(e.target.value);
              }}
              value={periode}
            />
          </Stack>
        </Stack>
      )}
    </>
  );
};

export default FilterByPeriode;
