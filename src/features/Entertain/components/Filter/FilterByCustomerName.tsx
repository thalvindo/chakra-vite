import { Box, Text } from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";
import Select from "react-select";
import useGetCustomerList from "../../../../services/useGetCustomerList";
import { CustomerModel } from "../../../../dataProviders/common/Customer/MapCustomerList";
import React from "react";
import ApprovalKlaimContext from "../../context/ApprovalKlaimFilterContext";

const FilterByCustomerName = ({ hasCustomerFilter }: { hasCustomerFilter: boolean }) => {
  const formContext = useFormContext();

  const { filterValues } = React.useContext(ApprovalKlaimContext);

  const [selectedCustomer, setSelectedCustomer] = React.useState<CustomerModel | null>(filterValues?.customer);

  const { data: customerList } = useGetCustomerList();


  return (
    <>
      {hasCustomerFilter && (
        <Box sx={{ width: "70%" }}>
          <Text sx={{ mb: 2 }}>Customer Name</Text>
          <Select
            options={customerList}
            onChange={(eachCustomer: CustomerModel | null) => {
              setSelectedCustomer(eachCustomer);
              formContext.setValue("customer", eachCustomer);
            }}
            value={selectedCustomer}
            getOptionLabel={(eachCustomer) => `${eachCustomer.name} - ${eachCustomer.code}`}
            getOptionValue={(eachRole) => eachRole.name}
            placeholder="Customer Name - Customer Code"
            isClearable
          />
        </Box>
      )}
    </>
  );
};

export default FilterByCustomerName;
