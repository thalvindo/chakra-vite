import React from "react";
import { Box, Stack, Text } from "@chakra-ui/react";
import Select from "react-select";
import { useFormContext } from "react-hook-form";
import { RoleModel } from "../../../../dataProviders/common/Role/MapRoleList";
import useGetNamaBawahanByRole from "../../../../services/useGetNamaBawahanByRole";
import useGetRoleList from "../../../../services/useGetRoleList";
import { UserModel } from "../../../../dataProviders/common/User/MapUserByRole";
import { isEmpty } from "lodash";
import ApprovalKlaimContext from "../../context/ApprovalKlaimFilterContext";

const FilterByJabatanAndBawahan = ({ hasJabatanAndBawahanFilter }: { hasJabatanAndBawahanFilter: boolean }) => {
  const { filterValues } = React.useContext(ApprovalKlaimContext);

  const [selectedRole, setSelectedRole] = React.useState<RoleModel | null>(filterValues?.jabatan);
  const [selectedBawahan, setSelectedBawahan] = React.useState<UserModel | null>(filterValues?.bawahan);

  const { data: roleList } = useGetRoleList();
  const { data: selectedNamaBawahanList } = useGetNamaBawahanByRole(selectedRole);

  const formContext = useFormContext();

  return (
    <>
      {hasJabatanAndBawahanFilter && (
        <Stack
          sx={{
            justifyContent: "space-between",
            display: "flex",
            gap: 4,
            flexDirection: "row",
            my: 5,
          }}
        >
          <Box sx={{ width: "35%" }}>
            <Text sx={{ mb: 2 }}>Jabatan</Text>
            <Select
              options={roleList}
              onChange={(eachRole: RoleModel | null) => {
                setSelectedRole(eachRole);
                formContext.setValue("jabatan", eachRole);
                setSelectedBawahan(null);
                formContext.setValue("bawahan", null);
              }}
              getOptionLabel={(eachRole: RoleModel) => eachRole.label}
              value={selectedRole}
              placeholder="Jabatan"
              getOptionValue={(eachRole) => eachRole.label}
              isClearable
            />
          </Box>
          <Box sx={{ width: "65%" }}>
            <Text sx={{ mb: 2 }}>Cari Nama</Text>
            <Select
              options={selectedNamaBawahanList}
              isDisabled={isEmpty(selectedRole)}
              onChange={(eachBawahan) => {
                setSelectedBawahan(eachBawahan);
                formContext.setValue("bawahan", eachBawahan);
              }}
              getOptionLabel={(eachBawahan) => `${eachBawahan.userName} - ${eachBawahan.nip}`}
              getOptionValue={(eachName) => eachName.nip}
              value={selectedBawahan}
              placeholder="Cari Nama"
              isClearable
            />
          </Box>
        </Stack>
      )}
    </>
  );
};

export default FilterByJabatanAndBawahan;
