import { Box, Text } from "@chakra-ui/react";
import { COMMON_STATUS, COMMON_STATUS_TYPE } from "../../../../constants/constants";
import Select from "react-select";
import { useFormContext } from "react-hook-form";
import React from "react";
import ApprovalKlaimContext from "../../context/ApprovalKlaimFilterContext";

const FilterByStatus = ({ hasStatusFilter }: { hasStatusFilter: boolean }) => {
  const formContext = useFormContext();

  const { filterValues } = React.useContext(ApprovalKlaimContext);

  const [selectedStatus, setSelectedStatus] = React.useState<COMMON_STATUS_TYPE | null>(filterValues?.statusApproval);

  return (
    <>
      {hasStatusFilter && (
        <Box sx={{ width: "30%" }}>
          <Text sx={{ mb: 2 }}>Status</Text>
          <Select
            options={COMMON_STATUS}
            onChange={(eachStatus) => {
              setSelectedStatus(eachStatus);
              formContext.setValue("statusApproval", eachStatus);
            }}
            value={selectedStatus}
            getOptionLabel={(eachStatus) => eachStatus.name}
            getOptionValue={(eachStatus) => eachStatus.name}
            placeholder="Status"
            isClearable
          />
        </Box>
      )}
    </>
  );
};

export default FilterByStatus;
