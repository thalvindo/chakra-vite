import React from "react";
import ApprovalKlaimRecordContext from "../context/ApprovalKlaimRecordContext";
import { Card, useTheme, Text, Stack } from "@chakra-ui/react";
import ActionButtonBasedOnLoggedUser from "./ActionButtonBasedOnLoggedUser";
import { CommonStatus } from "../../../models/common/Status";

const ApprovalCard = () => {
  const theme = useTheme();
  const { record } = React.useContext(ApprovalKlaimRecordContext);

  //harusnya lebih bagus, ini biar ngebut aja :))
  const RenderStatus = () => {
    return (
      <>
        {record?.entertainStatus === CommonStatus.Pending && (
          <Stack direction={"row"}>
            <Text>Status:</Text>
            <Text color={"orange"}>{record?.entertainStatus}</Text>
          </Stack>
        )}
        {record?.entertainStatus === CommonStatus.Rejected && (
          <Stack direction={"row"}>
            <Text>Status:</Text>
            <Text color={"red"}>{record?.entertainStatus}</Text>
          </Stack>
        )}
        {record?.entertainStatus === CommonStatus.Approved && (
          <Stack direction={"row"}>
            <Text>Status:</Text>
            <Text color={"blue"}>{record?.entertainStatus}</Text>
          </Stack>
        )}
        {record?.entertainStatus === CommonStatus.Processed && (
          <Stack direction={"row"}>
            <Text>Status:</Text>
            <Text color={"purple"}>{record?.entertainStatus}</Text>
          </Stack>
        )}
        {record?.entertainStatus === CommonStatus.Transferred && (
          <Stack direction={"row"}>
            <Text>Status:</Text>
            <Text color={"pink"}>{record?.entertainStatus}</Text>
          </Stack>
        )}
      </>
    );
  };
  return (
    <Card
      sx={{
        mt: 1.5,
        mb: 1.5,
        borderRadius: 4,
        py: 2,
        px: 2,
        backgroundColor: theme.colors.background.default,
      }}
    >
      <Text>ID : {record?.id}</Text>
      <RenderStatus />
      <Text>User pembuat : {record?.entertainCreatedBy.userName}</Text>
      <Text>Keterangan : {record?.keterangan}</Text>
      <Text>Customer Name : {record?.customerData.namaCustomer}</Text>
      <ActionButtonBasedOnLoggedUser />
    </Card>
  );
};

export default ApprovalCard;
