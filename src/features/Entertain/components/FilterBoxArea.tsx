import { Box, Stack, Text } from "@chakra-ui/react";
import React from "react";
import ApprovalKlaimContext from "../context/ApprovalKlaimFilterContext";

const FilterBoxArea = () => {
  const { filterValues } = React.useContext(ApprovalKlaimContext);
  return (
    // tar jadiin component si boxnya
    <Stack sx={{ display: "flex", justifyContent: "flex-start", flexDirection: "row" }}>
      {filterValues.periode && (
        <Box sx={{ border: "2px solid black", borderRadius: 4, paddingY: 0.6, paddingX: 1.6, mr: 1 }}>
          <Text>Periode </Text>
          <Text>{filterValues.periode.getStringDateFormat("MM-YYYY")}</Text>
        </Box>
      )}
      {filterValues.bawahan && (
        <Box sx={{ border: "2px solid black", borderRadius: 4, paddingY: 0.6, paddingX: 1.6, mr: 1 }}>
          <Text>Nama {filterValues.bawahan.roleName}</Text>
          <Text>{filterValues.bawahan.userName}</Text>
        </Box>
      )}
      {filterValues.statusApproval && (
        <Box sx={{ border: "2px solid black", borderRadius: 4, paddingY: 0.6, paddingX: 1.6, mr: 1 }}>
          <Text>Status Approval</Text>
          <Text>{filterValues.statusApproval.name}</Text>
        </Box>
      )}
      {filterValues.customer && (
        <Box sx={{ border: "2px solid black", borderRadius: 4, paddingY: 0.6, paddingX: 1.6, mr: 1 }}>
          <Text>Nama Customer</Text>
          <Text>{filterValues.customer.name}</Text>
        </Box>
      )}
    </Stack>
  );
};

export default FilterBoxArea;
