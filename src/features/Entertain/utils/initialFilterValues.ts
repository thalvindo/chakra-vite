import DateEthical from "../../../models/DateEthical";
import DateUtility from "../../../utils/DateUtility";
import ApprovalFilterValues from "../models/ApprovalFilterValues";

const filterInitialValue: ApprovalFilterValues = new ApprovalFilterValues({
  jabatan: null,
  bawahan: null,
  customer: null,
  periode: new DateEthical(DateUtility.getCurrentTimeAsEpoch()),
  statusApproval: null,
});

export default filterInitialValue;
