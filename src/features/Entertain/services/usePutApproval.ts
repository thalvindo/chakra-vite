import React from "react";
import { EntertainDataProvider } from "../dataProviders/EntertainDataProvider";
import { APPROVAL_CONSTANTS, APPROVAL_CONSTANTS_URL } from "../constants/constants";
import LoggedUser from "../../../models/common/LoggedUser";
import { useAppSelector } from "../../../redux/hooks";
import { ROLE_ID_SM, ROLE_ID_NSM, ROLE_ID_MARKETING_DIRECTOR } from "../../../constants/RoleConstants";
import RemoveDuplicateInArray from "../../../utils/RemoveDuplicateInArray";
import { KlaimEntertainModel } from "../models/KlaimEntertainListModelPack";

const usePutApproval = () => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const loggedUser: LoggedUser | null = useAppSelector((store) => store.authReducer.loggedUser);

  const handleOnChangeApprovalDecision = (approvalDecision: string) => {
    if (approvalDecision === APPROVAL_CONSTANTS.APPROVE) {
      return { approvalDecisionUrl: APPROVAL_CONSTANTS_URL.APPROVE, approvalDecisionBoolean: true };
    }
    if (approvalDecision === APPROVAL_CONSTANTS.REJECT) {
      return { approvalDecisionUrl: APPROVAL_CONSTANTS_URL.REJECT, approvalDecisionBoolean: false };
    }
    return { approvalDecisionUrl: "unknown", approvalDecisionBoolean: null };
  };

  const getApprovableRoleIds = (record: KlaimEntertainModel) => {
    let approvableRoleIds: Array<number> = [];

    approvableRoleIds.push(loggedUser?.userRoleId!!);

    if (record?.approvalDetail.isApprovableBySm && !record?.approvalDetail.isApprovedBySm) {
      approvableRoleIds.push(ROLE_ID_SM);
    }
    if (record?.approvalDetail.isApprovableByNsm && !record?.approvalDetail.isApprovedByNsm) {
      approvableRoleIds.push(ROLE_ID_NSM);
    }
    if (record?.approvalDetail.isApprovableByMarkDir && !record?.approvalDetail.isApprovedByMarkDir) {
      approvableRoleIds.push(ROLE_ID_MARKETING_DIRECTOR);
    }

    approvableRoleIds = RemoveDuplicateInArray(approvableRoleIds);

    return approvableRoleIds;
  };

  const getPayload = (approvableRoleIds: Array<number>, approvalDecisionBoolean: boolean | null) => {
    let payload = {};

    if (approvableRoleIds.find((id) => id === ROLE_ID_MARKETING_DIRECTOR)) {
      payload = { ...payload, mark_dir_approve: approvalDecisionBoolean };
    }

    if (approvableRoleIds.find((id) => id === ROLE_ID_NSM)) {
      payload = { ...payload, nsm_approve: approvalDecisionBoolean };
    }

    if (approvableRoleIds.find((id) => id === ROLE_ID_SM)) {
      payload = { ...payload, sm_approve: approvalDecisionBoolean };
    }

    return payload;
  };

  const putApprovalEntertain = async (record: KlaimEntertainModel, approvalDecision: string) => {
    const { approvalDecisionUrl, approvalDecisionBoolean } = handleOnChangeApprovalDecision(approvalDecision);
    const approvableRoleIds = getApprovableRoleIds(record);
    const payload = getPayload(approvableRoleIds, approvalDecisionBoolean);

    try {
      setIsLoading(true);

      const data: string = await EntertainDataProvider.putApprovalEntertain(record.id, approvalDecisionUrl, payload);
      return data;
    } catch (error: unknown) {
      console.log("error", error);
      return error;
    } finally {
      setIsLoading(false);
    }
  };

  return { putApprovalEntertain, isLoading };
};

export default usePutApproval;
