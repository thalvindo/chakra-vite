import React from "react";
import { EntertainDataProvider } from "../dataProviders/EntertainDataProvider";
import { KlaimEntertainModel } from "../models/KlaimEntertainListModelPack";

const usePostProcessApprovalMsd = () => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const postProcessApproval = async (record: KlaimEntertainModel, payload: any) => {
    const updatedPayload = {
      ...payload,
      or_process_date: parseInt(JSON.stringify(payload.or_process_date)),
    };

    try {
      setIsLoading(true);

      const data: string = await EntertainDataProvider.postProcessApprovalMsd(record.id, updatedPayload);
      return data;
    } catch (error: unknown) {
      console.log("error", error);
      return error;
    } finally {
      setIsLoading(false);
    }
  };

  return { postProcessApproval, isLoading };
};

export default usePostProcessApprovalMsd;
