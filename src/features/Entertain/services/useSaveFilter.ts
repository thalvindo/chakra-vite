import { useState } from "react";
import { EntertainDataProvider } from "../dataProviders/EntertainDataProvider";
import ApprovalFilterValues from "../models/ApprovalFilterValues";

const useSaveFilter = () => {
  const [message, setMessage] = useState<string>("");
  const [isLoading, setLoading] = useState<boolean>(true);

  const saveData = async (filterValues: ApprovalFilterValues) => {
    setLoading(true);
    try {
      const savedFilter = {
        ...filterValues,
        periode: filterValues.periode?.getEpochDate(),
      };
      const result: string = await EntertainDataProvider.saveFilter(savedFilter);

      setMessage(result);
    } catch (error: unknown) {
      setMessage(error as string);
    } finally {
      setLoading(false);
    }
  };

  return { message, isLoading, saveData };
};

export default useSaveFilter;
