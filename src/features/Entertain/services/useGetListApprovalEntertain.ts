import { useEffect, useState } from "react";
import { AxiosError } from "axios";
import { EntertainDataProvider } from "../dataProviders/EntertainDataProvider";
import { KlaimEntertainModel, KlaimEntertainListMetaDataModel } from "../models/KlaimEntertainListModelPack";
import { AXIOS_MESSAGE_RESPONSE } from "../../../constants/constants";
import ApprovalFilterValues from "../models/ApprovalFilterValues";

const useGetEntertainList = (filterValues: ApprovalFilterValues, selectedMetaData: { page: number; limit: number }) => {
  const [data, setData] = useState<Array<KlaimEntertainModel>>([]);
  const [metaData, setMetaData] = useState<KlaimEntertainListMetaDataModel | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);
  const [counter, setCounter] = useState<number>(0);

  const handleOnCatch = (error: unknown) => {
    if (error instanceof AxiosError) {
      setError(error.response?.data.message);
      return;
    }
    if (error instanceof Error) {
      setError(error.message);
      return;
    }
    setError(error as string);
  };

  useEffect(() => {
    const getData = async () => {
      setLoading(true);
      const payload = {
        // buat testing, ntar start_datenya ganti 1672506000
        start_date: JSON.stringify(filterValues.periode.getStartOfMonth()),
        end_date: JSON.stringify(filterValues.periode.getEndOfMonth()),
        customer_id: filterValues.customer?.id ?? null,
        user_id: filterValues.bawahan?.userId ?? null,
        status: filterValues.statusApproval?.id ?? null,
        page: selectedMetaData.page,
        limit: selectedMetaData.limit,
      };
      try {
        const result = await EntertainDataProvider.getList(payload);

        if (result.message === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE && result.status === AXIOS_MESSAGE_RESPONSE.SUCCESS_GET_STATUS) {
          setData(result.data.records);
          setMetaData(result.data.metadata);
          return;
        }
        setData([]);
        setMetaData(null);
      } catch (error: unknown) {
        handleOnCatch(error);
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, [filterValues, counter, selectedMetaData.page, selectedMetaData.limit]);

  const refetch = () => {
    setCounter((counterBefore) => counterBefore + 1);
  };

  return { data, metaData, error, isLoading, refetch };
};

export default useGetEntertainList;
