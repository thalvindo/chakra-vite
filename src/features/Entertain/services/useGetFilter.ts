import React, { useState } from "react";
import { EntertainDataProvider } from "../dataProviders/EntertainDataProvider";
import ApprovalFilterValues from "../models/ApprovalFilterValues";
import DateEthical from "../../../models/DateEthical";
import { isEmpty } from "lodash";

const useGetFilter = () => {
  const [data, setData] = useState<ApprovalFilterValues | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  React.useEffect(() => {
    const getData = async () => {
      setLoading(true);

      try {
        const result: ApprovalFilterValues = await EntertainDataProvider.getSavedFilter();
        if (isEmpty(result)) {
          setData(result);
        }
        if (!isEmpty(result)) {
          setData({
            ...result,
            periode: new DateEthical(result.periode as unknown as number),
          });
          return;
        }
      } catch (error: unknown) {
      } finally {
        setLoading(false);
      }
    };

    getData();
  }, []);

  return { data, isLoading };
};

export default useGetFilter;
