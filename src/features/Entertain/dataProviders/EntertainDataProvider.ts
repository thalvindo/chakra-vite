import { AxiosError, AxiosResponse } from "axios";
import { AXIOS_MESSAGE_RESPONSE, ERRORS, ETHICAL_API_VERSION } from "../../../constants/constants";
import AxiosBaseClient from "../../../dataProviders/config/AxiosBaseClient";
import { KlaimEntertainGetListModelPack } from "../models/KlaimEntertainListModelPack";
import IndexedDatabase from "../../../database/IndexDB";
import { isEmpty } from "lodash";

export const EntertainDataProvider = {
  getList: async (param: any) => {
    // const url = `/v1/header/approval/list?page=${params.pagination.page}&limit=${params.pagination.perPage}`;
    //https://develop-api.pharos.co.id/ethical/v1/entertain/approval?page=1&limit=10&start_date=1698771600&end_date=1701363599
    const response: AxiosResponse = await AxiosBaseClient.get(ETHICAL_API_VERSION + "/entertain" + "/approval", {
      params: param,
    });
    const dataPack = new KlaimEntertainGetListModelPack(response.data);

    if (response.status === 200) return Promise.resolve(dataPack);
    return Promise.reject(new Error(ERRORS.ERROR_WHEN_FETCHING_DATA));
  },
  saveFilter: async (filterValues: any) => {
    if (!IndexedDatabase.isOpen()) await IndexedDatabase.open();
    try {
      const databaseResponse = await IndexedDatabase.approvalKlaimFilter.toArray();

      if (isEmpty(databaseResponse)) {
        await IndexedDatabase.approvalKlaimFilter.add(filterValues);
        return Promise.resolve(AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE);
      } else if (databaseResponse && databaseResponse.length === 1) {
        await IndexedDatabase.approvalKlaimFilter.update(databaseResponse[0].id, filterValues);
        return Promise.resolve(AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE);
      }

      await IndexedDatabase.approvalKlaimFilter.clear();
      throw "error keapus nih.";
    } catch (error: unknown) {
      return Promise.reject("error");
    }
  },
  getSavedFilter: async () => {
    if (!IndexedDatabase.isOpen()) await IndexedDatabase.open();
    const databaseResponse = await IndexedDatabase.approvalKlaimFilter.toArray();
    if (databaseResponse && !isEmpty(databaseResponse) && databaseResponse.length === 1) {
      return Promise.resolve(databaseResponse[0]);
    }
    return Promise.resolve(null);
  },
  putApprovalEntertain: async (entertainId: number, approvalDecision: string, param: any) => {
    try {
      const response: AxiosResponse = await AxiosBaseClient.put(
        ETHICAL_API_VERSION + "/entertain" + `/${approvalDecision}` + `/${entertainId}`,
        param
      );

      if (response.data.message === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE) {
        return Promise.resolve(response.data.message);
      }
      return Promise.resolve(response);
    } catch (error: unknown) {
      if (error instanceof AxiosError) {
        return Promise.reject(error.response?.data.message);
      }
      return Promise.reject(error);
    }
  },
  postProcessApprovalMsd: async (entertainId: number, param: any) => {
    try {
      const response: AxiosResponse = await AxiosBaseClient.post(ETHICAL_API_VERSION + "/entertain" + `/process` + `/${entertainId}`, param);
      if (response.data.message === AXIOS_MESSAGE_RESPONSE.SUCCESS_MESSAGE) {
        return Promise.resolve(response.data.message);
      }
      return Promise.resolve(response);
    } catch (error: unknown) {
      if (error instanceof AxiosError) {
        return Promise.reject(error.response?.data.message);
      }
      return Promise.reject(error);
    }
  },
};
